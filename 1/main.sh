#!/usr/bin/env bash
INFILE="input.txt"
OUTFILE="output.txt"
read -r MONTH<${INFILE}
array=(January February March April May June July August September October November December)
echo ${array[(($MONTH%12))]} > ${OUTFILE}