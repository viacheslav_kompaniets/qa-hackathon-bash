#!/usr/bin/env bash
CURRENT_DIR=`pwd`
INFILE="input.txt"
OUTFILE="output.txt"
SUM=1

while read line
do
    VARIANTS=`echo $line | awk -F',' '{print $2}'`
    SUM=$((SUM*($VARIANTS+1)))
done < "$INFILE"

echo $((SUM-1)) > ${OUTFILE}