#!/usr/bin/env bash
CURRENT_DIR=`pwd`
INFILE="input.txt"
OUTFILE="output.jpg"
read -r POSTER<${INFILE}
TITLE=$(echo ${POSTER} | sed 's/ /+/g;s/!/%21/g;s/"/%22/g;s/#/%23/g;s/\&/%26/g;s/'\''/%28/g;s/(/%28/g;s/)/%29/g;s/:/%3A/g;s/\//%2F/g');
IMAGE=`curl -s -k "http://www.omdbapi.com/?t=${TITLE}&y=&plot=short&r=json" | jq '.Poster' | sed 's/"//g'`
wget ${IMAGE} -O ${CURRENT_DIR}/${OUTFILE} 2>&1