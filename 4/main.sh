#!/usr/bin/env bash
INFILE="input.txt"
OUTFILE="output.txt"
cat ${INFILE} | grep Chrome | awk '{s+=$4} END {print s}' > ${OUTFILE}